import java.util.concurrent.ConcurrentHashMap;

public class concurrethashmap {
    public static void main(String[] args)
    {
        ConcurrentHashMap<Integer, String> current = 
                   new ConcurrentHashMap<Integer, String>();
                   current.put(100, "Hello");
                   current.put(101, "Hi");
                   current.put(102, "Hey");
 
        System.out.println(current);
 
        current.putIfAbsent(101, "Hello");
 
        System.out.println(current);
 
        current.remove(101, "Hi");
 
        System.out.println(current);
 
        current.replace(100, "Hello", "For");

        System.out.println(current);
    }
}
