import java.util.*;

public class hashset {
    public static void main(String[] args) {
        HashSet<String> hashset = new HashSet<String>();

        hashset.add("Thailand");
        hashset.add("England");
        hashset.add("South Africa");
        hashset.add("Thailand");

        System.out.println(hashset);

        System.out.println(hashset.contains("Thailand"));

                hashset.remove("England");

        System.out.println(hashset);

        System.out.println("\nIterating over list:");
        Iterator<String> i = hashset.iterator();
        while (i.hasNext())
            System.out.println(i.next());
    }
}
