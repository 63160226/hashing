import java.util.LinkedHashMap;

public class linkhashmap {
    public static void main(String a[]) {
        LinkedHashMap<String, String> link = new LinkedHashMap<String, String>();
        link.put("one", "Hello.Mr");
        link.put("two", "Hi.Mrs");
        link.put("four", "Hey");

        System.out.println(link);
        System.out.println(link.get("one"));
        System.out.println(link.size());
        System.out.println(link.isEmpty());
        System.out.println(link.containsKey("two"));
        System.out.println(link.containsValue("hi" + ".Mrs"));
        System.out.println(link.remove("one"));
        System.out.println(link);
    }
}
